import { css } from "@emotion/react";

// Font size
export const tinyFontSize = '11px'
export const mediumFontSize = '12px'
export const primaryFontSize = '15px'
export const secondaryFontSize = '18px'
export const tertiaryFontSize = '21px'

// Font family
export const defaultFontFamily = `'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif`


// Font color
export const defaultFontColor = '#707070';
export const greyColor = '#CECECE';
export const lightGreyColor = '#F8F9FB';
export const blueColor = '#387BAA';
export const blueHoverColor = '#296189';
export const whiteColor = '#FFF';
export const blackColor = '#000';
export const lightGreyFontColor = '#4A4A4A';
export const borderGrey = '#E0E0E0';
export const borderGrey2 = '#70707059';


const GlobalStyles = css`
    @import url('https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap');
    html,body {
        margin: 0;
        padding: 0;
    };
    
    * {
        box-sizing: border-box;
    };

    img {
        max-width: 100%;
    }
    
    a {
        color: #000;
    };

    h1{
        font-size: 32px; 
        line-height:1.125em;
        margin: 0 0 0.5625em;
    };

    h2{
        font-size: 28px;
        line-height:36px; 
        margin-top:0.9em 0;
    };

    h3{
        font-size: 23px; 
        line-height: 28px; 
        margin-top: 1.125em 0;
    };
    
    h4 {
        font-size: 15px; 
        line-height: 19px; 
        margin-top: 1.125em 0;
    };
    
    body {
        margin: 0;
        font-family: ${defaultFontFamily};
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        font-size: ${primaryFontSize};
        color: ${defaultFontColor};
    };

    .p-badge {
        font-size: ${mediumFontSize};
        font-weight: normal;
        min-width: 23px;
        height: 18px;
        line-height: 18px;
        border-radius:10px
    }
    ul.unlist {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    .p-dialog .p-dialog-header .p-dialog-title {
        font-size: 28px;
        color: ${lightGreyFontColor};
    }

    // Dialog
    .overlayColor {
        background-color: rgba(206,206,206, 0.45) !important;
    }

    .p-dialog-header-close-icon {
        font-size: 24px;
        color: ${blackColor};
    }

    .p-dialog {
        box-shadow: 0px 3px 6px #00000029;
        border: 1px solid #F8F9FB;
        border-radius: 13px;
        overflow: hidden;
    }
    
    .p-dialog-header.p-dialog-header {
        padding: 30px;
    }

    .p-dialog-content.p-dialog-content  {
        padding: 0;
    }

    // tab theme
    .p-tabview-nav-content {
        border-bottom: 2px solid ${greyColor};
    }
    .p-tabview-nav.p-tabview-nav  {
        margin: 0 30px 0 7px; 
        border: 0;
        position: relative;
    }
    .cancel-button {
        color: ${defaultFontColor};
        background-color: ${greyColor}; 
        border: ${greyColor};
        &:hover,&:focus {
            background-color: ${greyColor} !important; 
            border: ${greyColor} !important;
            color: ${defaultFontColor} !important;
        }
    }

    .p-tabview {
        .p-tabview-nav .p-tabview-ink-bar {
            background-color: ${blueColor}; 
            display: none;
        }

        .p-tabview-nav li {
            padding: 0 23px;
            .p-tabview-nav-link {
                color: ${lightGreyFontColor};
                font-size: ${secondaryFontSize};
                padding: 0 0 12px;
            }
    
            &.p-highlight .p-tabview-nav-link {
                color: ${defaultFontColor};
                position: relative;
                &::before {
                    content: '';
                    z-index: 1;
                    display: block;
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    height: 2px;
                    width: 100%;
                    background-color: ${blueColor}; 
                }
            }
    
            .p-tabview-nav-link:not(.p-disabled):focus {
                box-shadow: none;
            }
    
        }

        .p-tabview-panels {
            padding: 30px;
            p:first-of-type {
                margin-top: 0;
                padding-top: 0;
            }
        }

        // file uploader
        .p-fileupload .p-fileupload-content {
            border: 4px dashed ${borderGrey2};
            border-radius: 11px;
            padding: 97px 0 117px;
            text-align: center;
        }

        .fs-18 {
            font-size: 18px; 
        }

        .hidden {
            display: none;
        }

        .button-primary {
            font-size: 16px;
            color: ${whiteColor};
            background-color: ${blueColor};
            padding: 10px 15px;
            border-color:${blueColor};
            &:hover,&:focus {
                background-color: ${blueColor} !important;
                border-color: ${blueColor} !important;
            }
            & .p-button-label {
                font-weight: normal;
            }
        }
        .p-fileupload {
            position: relative;
        }
        .p-fileupload-choose {
            position: absolute;
            bottom: 117px;
            left: 50%;
            transform: translateX(-50%);
            z-index: 1;
        }

        .emptyTemplate {
            padding-bottom: 40px;
        }

        .ml-20 {
            margin-left: 20px;
        }
        .mr-20 {
            margin-right: 20px;
        }
    }
`;

export default GlobalStyles;
