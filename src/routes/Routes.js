import React from "react";
import { Global } from "@emotion/react";
import {
    BrowserRouter,
    Routes,
    Route
  } from "react-router-dom";

import FileUploader from "containers/FileUploader/index";
import GlobalStyles from 'GlobalStyles';

/**
 * Routes
 * @returns Rotes
 */
const HomeRoutes = () => {
	return (
		<>
			<BrowserRouter>
			<Global styles={GlobalStyles} />
				<Routes>
					<Route path="/" element={<FileUploader />} />
				</Routes>
			</BrowserRouter>
		</>
	);
}

export default HomeRoutes;
