
export async function getImages() {
	return await fetch('https://jsonplaceholder.typicode.com/photos')
		.then((response) => response.json())
		.then((data) => data)
        .catch((err) => console.log(err))
}
