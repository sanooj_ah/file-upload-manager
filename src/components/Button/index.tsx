import React, { MouseEventHandler } from "react";
import { ButtonPositionType } from "primereact/button";
import { ButtonStyles } from "./indexStyles";

// hutton compoenent
const PrimaryButton = ({
	label = "Save",
	icon,
	iconPos,
	onClick,
    className="p-button-default"
}: {
	label: string;
	icon?: string;
	iconPos?: ButtonPositionType | undefined;
	onClick?: MouseEventHandler<HTMLButtonElement>;
    className?: string
}): JSX.Element => {
	return (
		<ButtonStyles
			label={label}
			icon={icon}
			iconPos={iconPos}
			onClick={onClick}
            className={className}
		></ButtonStyles>
	);
};

export default PrimaryButton;
