import styled from "@emotion/styled";
import { blueColor, blueHoverColor } from "GlobalStyles";
import { Button } from "primereact/button";

export const ButtonStyles = styled(Button)`
    background-color: ${props => props.className === 'p-button-default' && blueColor  };
    border-color: ${props => props.className === 'p-button-default' && blueColor  };
    font-size: 20px;
    &:hover {
        background-color: ${props => props.className === 'p-button-default' && blueHoverColor  } !important;
        border-color: ${props => props.className === 'p-button-default' && blueHoverColor  } !important ;
    }
`;
