import React, { useRef } from 'react';
import { Toast } from 'primereact/toast';

// Toast HOC component
const ToastHOC = (WrappedComponent:any) => function Comp(props:any) {

    // Toast reference
	const toast = useRef(null);

    // toast trigger
    const toastMessage = (severity ="error", summary = "Error", detail:string) => {
        toast.current.show({severity: severity, summary: summary, detail: detail});
    }
    return (
        <>
            <Toast ref={toast}></Toast>
            <WrappedComponent {...props} toastMessage={toastMessage} />
        </>
    )
}

export default ToastHOC;
