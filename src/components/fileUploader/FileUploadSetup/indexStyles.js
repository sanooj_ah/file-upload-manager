import styled from "@emotion/styled";
import { borderGrey, lightGreyColor } from "GlobalStyles";

export const UnOrderedStyle = styled.ul`
	list-style: none;
    padding: 0;
    margin: 0 0 20px;
    display: flex;
    flex-wrap: wrap;
    & li {
        width: 96px;
        height: 104px;
        margin-right: 10px;
        img {
            height: 100%;
            object-fit: cover;
        }
    }
`;

export const UploadButtonStyle = styled.div`
	background-color: ${lightGreyColor};
    border: solid 1px ${borderGrey};
    border-radius: 13px;
    width: 96px;
    height: 104px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    & i {
        font-size: 24px;
    }
`;