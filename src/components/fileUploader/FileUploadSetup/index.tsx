import React, { forwardRef, MouseEventHandler, useEffect, useImperativeHandle, useState } from 'react'
import { ImageFile } from 'FileUpload.interface';
import { UnOrderedStyle, UploadButtonStyle } from './indexStyles';

// uploaded file listing with upload popup option
const FileUploadSetup = forwardRef((props:any,ref:any):JSX.Element => {

    
    // attached file sate
    const [image, setImage] = useState<ImageFile>(null);

    //getting changes of uploaded file
    useImperativeHandle(ref, () => ({
      changeDetect() {
          const localStoredImage = JSON.parse(window.sessionStorage.getItem("uploadedImage"));
          setImage(localStoredImage)
        }
    }));

    return (
        <>
          <UnOrderedStyle>
            {image&&image.objectURL && (
              <li><img src={image.objectURL} alt="Uploaded File" /></li>
            )}
            <li>
                <UploadButtonStyle onClick={props.showGalleryPopup}>
                    <i className='pi pi-plus'></i>
                </UploadButtonStyle>
            </li>
          </UnOrderedStyle>  
        </>
    )
})

export default FileUploadSetup;
