import styled from "@emotion/styled";
import {  defaultFontColor } from "GlobalStyles";
import { Tag } from "primereact/tag";

export const H3 = styled.h3`
	font-size: 18px;
    color: ${defaultFontColor};
    margin-bottom: 0;
`;

export const P = styled.p`
	font-size: 14px;
    color: ${defaultFontColor};
    margin-bottom: 16px;
`;

export const ImageBlockStyles = styled.div`
	width: 200px !important;
    flex: none !important;
`;

export const TextBlockStyles = styled.div`
    flex: 1 !important;
    text-align: left;
`;
export const TagStyles = styled(Tag)`
    flex: none !important;  
`;
