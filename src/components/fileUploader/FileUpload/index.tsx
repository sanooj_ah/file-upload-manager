import React, {useEffect, useState } from "react";
import { FileUpload } from "primereact/fileupload";
import { H3, ImageBlockStyles, P, TagStyles, TextBlockStyles } from "./indexStyles";
import fileUploadImage from 'assets/images/file-upload-image.png'
import { Button } from "primereact/button";
import { ImageFile } from "FileUpload.interface";
import ToastHOC from "components/ToastHOC";

// image upload component
const FileUploader = ({fileUpload, toastMessage}:{fileUpload?:any, toastMessage:any}):JSX.Element => {
    
	//Checking upload button
	const [enableUploadButton, setEnableUploadButton] = useState(true);

	//uploaded file
	const [uploadedFile, setUploadedFile] = useState(null)
	
	// file upload file emitting to parent
	useEffect(() => {
		fileUpload(uploadedFile);
	}, [uploadedFile])
	
	const detectFileUploadChange = (file:ImageFile) => {
		setUploadedFile(file);
	}
	// on select image
	const onSelect = () => {
        setEnableUploadButton(false);	
		toastMessage('success', 'Success', 'File Attached');
	};

	// template remove
    const onTemplateRemove = (callback:any) => {
        setEnableUploadButton(true)
        callback();
    };

	// template for image attach
	const itemTemplate = (file: ImageFile, props: any) => {
		detectFileUploadChange(file);
		return (
                <>
				    <ImageBlockStyles>
					<img
						alt={file.name}
						role="presentation"
						src={file.objectURL}
						width={100}
					/>
                    </ImageBlockStyles>
					<TextBlockStyles>
						{file.name}
					</TextBlockStyles>
                    <TagStyles
                        value={props.formatSize} 
                        severity="warning"
                    />
                    <Button
                        type="button"
                        icon="pi pi-times"
                        className="p-button-outlined p-button-rounded p-button-danger ml-20  mr-20"
                        onClick={() => onTemplateRemove(props.onRemove)} 
                        />
                </>
		);
	};

	// template for image upload
	const emptyTemplate = () => {
		return (
			<div className="p-d-flex p-ai-center p-dir-col emptyTemplate">
                <img src={fileUploadImage} alt="" />
				<H3>DROP FILES HERE OR CLICK TO UPLOAD</H3>
				<P>
					Drag files and folders here to upload or click here to browse image
					from your computer
				</P>
			</div>
		);
	};

	// choose option button syle
    const chooseOptions = {
        label:"Select a photo from your computer",
        icon: 'none',
        className: "button-primary"
    };

	return (
		<>
			<FileUpload
				name="demo[]"
                onSelect={onSelect}
				accept="image/*"
				maxFileSize={1000000}
				headerTemplate={(options => enableUploadButton ? options.chooseButton : <></>)}
				itemTemplate={itemTemplate}
				emptyTemplate={emptyTemplate}
                chooseOptions={chooseOptions}
			/>
		</>
	);
};

export default ToastHOC(FileUploader);
