import styled from "@emotion/styled";

export const UnOrderedStyle = styled.ul`
	list-style: none;
    padding: 0;
    margin: 0 0 20px;
    display: flex;
    flex-wrap: wrap;
    & li {
        width: 164px;
        height: 164px;
        margin-right: 15px;
        cursor: pointer;
        img {
            height: 100%;
            object-fit: cover;
        }
    }
`;