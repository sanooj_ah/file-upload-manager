import React, { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { TabPanel, TabView } from "primereact/tabview";
import { ProgressSpinner } from 'primereact/progressspinner';

import { getImages } from "services/jsonApi.service";
import FileUploader from "../FileUpload";
import { UnOrderedStyle } from "./indexStyles";
import { Image } from "FileUpload.interface";


// popup image select
const SelectImage = forwardRef((props:any, ref:any):JSX.Element => {

    // state for image list
    const [images, setImages] = useState([]);

    // uploaded file setup
    const [uploadedFile, setUploadedFile] = useState(null)
    
    // refering parent button trigger ( saving to the session storage)
    useImperativeHandle(ref, () => ({
        saveFile() {
            if(uploadedFile) {
                window.sessionStorage.setItem("uploadedImage",JSON.stringify(uploadedFile));
            }
        }
    }));
    
    // service reference for the images
    useEffect(() => {
        getImages().then((images:Image[]) => {
            setImages(images)
        })
    }, []);

    // getting uploaded file
    const getFile = (file:File) => {
        setUploadedFile(file);
    }
	return (
		<TabView>
            <TabPanel header='Gallery'>
                <UnOrderedStyle>
                    {images.length ? images.map((images:Image,index: number): (JSX.Element | false) => (
                        index <= 2 && <li key={index}><img src={images.url} alt="" /></li>
                    )): <ProgressSpinner style={{width: '50px', height: '50px'}} strokeWidth="3" fill="var(--surface-ground)" animationDuration=".5s"/>}
                </UnOrderedStyle>
            </TabPanel>
            <TabPanel header='Upload'>
                <FileUploader fileUpload={getFile}/>
            </TabPanel>
        </TabView>
	);
});

export default SelectImage;
