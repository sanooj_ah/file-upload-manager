import styled from "@emotion/styled";
import { greyColor, tertiaryFontSize } from "GlobalStyles";
import { Link } from 'react-router-dom';

const metaIcon = `
    margin: 0 10px;
    position: relative;
    width: 18px;
    height: 18px;
    display: inline-block;
`

const badgeStyle = `
    position: absolute;
    bottom: 90%;
    left: 100%;
    transform: translate(-50%);
`
export const HeaderWrapper = styled.div`
	display: flex;
	width: 100%;
    padding: 20px 24px;
    justify-content: space-between;
`;

export const MetaPanel = styled.div`
    display: flex;
    align-items: center;
`
export const ChatIcon = styled(Link)`
	${metaIcon}
    & i {
        font-size: 18px;
    }
    & .p-badge {
        ${badgeStyle}
    }
`;

export const BellIcon = styled(Link)`
    ${metaIcon}
    & i {
        font-size: 18px;
    }
    & .p-badge {
        ${badgeStyle}
    }
`;

export const ProfilePanel = styled.div`
    background: ${greyColor} 0% 0% no-repeat padding-box;
    width: 48px;
    height: 48px;
    font-size: ${tertiaryFontSize};
    margin: 0 10px;
    border-radius:50%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #707070;
    font-weight: bold
`
