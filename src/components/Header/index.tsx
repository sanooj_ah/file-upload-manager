import React from 'react';
import { Badge } from 'primereact/badge';
import { Link } from 'react-router-dom';
import { HeaderWrapper, BellIcon, ChatIcon, MetaPanel, ProfilePanel } from './indexStyles';
import Logo from 'assets/images/logo.png';

const Header = (): JSX.Element => {
    return (
        <HeaderWrapper>
            <Link to="/">
                <img src={Logo} alt="File Manager upload" />
            </Link>
            <MetaPanel>
                <ChatIcon to="/" >
                    <Badge value={5} severity="danger"/>
                    <i className='pi pi-comment'></i>
                </ChatIcon>
                <BellIcon to="/">
                    <Badge value={10}  severity="danger"/>
                    <i className='pi pi-bell'></i>
                </BellIcon>
                <ProfilePanel>MR</ProfilePanel>
            </MetaPanel>
        </HeaderWrapper>
    )
}

export default Header;
