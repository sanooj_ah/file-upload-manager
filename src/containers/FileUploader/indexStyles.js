import styled from "@emotion/styled";
import { blueColor, greyColor, lightGreyColor, lightGreyFontColor, whiteColor } from "GlobalStyles";

export const FileUploaderStyles = styled.section`
	display: grid;
	grid-template-rows: 80px auto;
	width: 100%;
	height: 100vh;
`;

export const FileUploadContainerStyles = styled.div`
	background-color: ${lightGreyColor};
	padding: 67px;
	width: 100%;
	display: flex;
	justify-content: center;
`;

export const FileUploadWrapperStyles = styled.div`
	width: 100%;
	max-width: 1088px;
	border-radius: 13px;
	display: grid;
	grid-template-columns: auto 316px;
`;

export const FileUploadPanelStyles = styled.div`
	background-color: ${whiteColor};
	box-shadow: 0px 3px 6px #00000029;
	border: 1px solid #F8F9FB;
	border-radius: 13px 0 0 13px;
	padding: 45px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;
export const FileUploadAsideStyles = styled.div`
	background-color: ${blueColor};
	border-radius: 0 13px 13px 0;
`;

export const HeadingWrapStyles = styled.div`
	padding-bottom: 20px;
	position: relative;
	margin-bottom: 30px;
	&::before {
		content: '';
		display: block;
		height: 5px;
		width: 100px;
		background-color: ${blueColor};
		position: absolute;
		bottom: 0;
		left: 0;
		border-radius: 10px;
	}
`

export const MainHeadingStyles = styled.h2`
	margin: 0 0 10px;
	padding: 0;
	color: ${lightGreyFontColor};
`
export const SubHeadingStyles = styled.h4`
	margin: 0;
	padding: 0;
`
export const FooterStyles = styled.div`
	border-top: ${greyColor} 1px solid;
	position: relative;
	margin-left: -30px;
	margin-right: -30px;
	padding:30px
`

