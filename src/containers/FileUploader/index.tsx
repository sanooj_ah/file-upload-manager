import React, { useRef, useState } from 'react';
import { Dialog } from 'primereact/dialog';
import FileUploadSetup from 'components/fileUploader/FileUploadSetup';
import PrimaryButton from 'components/Button';
import Header from 'components/Header';
import {FileUploadAsideStyles, FileUploadContainerStyles, FileUploaderStyles, FileUploadPanelStyles, FileUploadWrapperStyles, FooterStyles, HeadingWrapStyles, MainHeadingStyles, SubHeadingStyles} from './indexStyles';
import SelectImage from 'components/fileUploader/SelectImage';

/**
 * File uploader component
 */
const FileUploader = (): JSX.Element => {

    //select image (child) reference
    const childRef = useRef(null);
    const uploadRef = useRef(null);

    // getting gallery status
    const [galleryStatus, setGalleryStatus] = useState<boolean | boolean>(false);

    // show Gallery popup
    const showGallery = () => {
        setGalleryStatus(!galleryStatus)
    }

    // hide Gallery popup
    const onHide = () => {
        setGalleryStatus(!galleryStatus)
    }

    // when attach the file
    const onSelect = () => {
        childRef.current.saveFile();
        uploadRef.current.changeDetect();
        setGalleryStatus(!galleryStatus);
    }

    // footer template
    const footer = (
        <FooterStyles>
            <PrimaryButton label="Close" className='cancel-button' onClick={onHide} />
            <PrimaryButton label="Select" onClick={onSelect} />
        </FooterStyles>
    );

    return (
        <>
        <FileUploaderStyles>

            <Header />
            
            <FileUploadContainerStyles>
                <FileUploadWrapperStyles >
                    <FileUploadPanelStyles>
                        <div>
                            <HeadingWrapStyles>
                                <MainHeadingStyles>Set up your WorkPerk</MainHeadingStyles>
                                <SubHeadingStyles>Work perks set up will only take a minutes</SubHeadingStyles>
                            </HeadingWrapStyles>
                            
                            <FileUploadSetup showGalleryPopup={showGallery} ref={uploadRef} />
                        </div>
                        <div>
                            <PrimaryButton label="Next" />
                        </div>
                    </FileUploadPanelStyles>
                    <FileUploadAsideStyles>&nbsp;</FileUploadAsideStyles>
                </FileUploadWrapperStyles>
            </FileUploadContainerStyles>
        </FileUploaderStyles>
        
        {/* Dialog */}
        <Dialog  maskClassName="overlayColor" visible={galleryStatus} header="Select Image" footer={footer} onHide={onHide} style={{width: '50vw', minWidth: '1200px'}}>
            <SelectImage ref={childRef}/>
        </Dialog>
        </>
    )
}

export default FileUploader;
