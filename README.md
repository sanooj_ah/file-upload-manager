# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* File upload manager

### How do I get set up? ###

* Use React 17.0.2 version
* Run file using `npm start`
* Dependencies
  `"@emotion/react": "^11.7.1",`
  `"@emotion/styled": "^11.6.0",`
  `"react-router-dom": "^6.2.1",`
  `"typescript": "^4.5.4",`
  `"primeicons": "^5.0.0",`
  `"primereact": "^7.1.0",`
* Run tests using npm test
* For build `npm build`

### comments about the project

* currently uploaded file used session storage ( we can also consider state management like Redux(Redux tools) or Context)
* Image gallery used `https://jsonplaceholder.typicode.com/photos` api